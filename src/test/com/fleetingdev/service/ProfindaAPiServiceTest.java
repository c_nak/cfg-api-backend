package com.fleetingdev.service;

import com.profindaapi.*;
import org.apache.http.HttpResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProfindaAPiServiceTest {

    private UserActions userActions;
    private HttpResponse mockedResponse;

    @Before
    public void setUp() throws Exception {
        userActions = mock(UserActions.class);
        mockedResponse = mock(HttpResponse.class);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void updateByEmail() throws Exception {
        String userDoesNotExist = "userDoesNotExist@domain.com";
        String user2 = "user2@domain.com";
        String user3 = "user3@domain.com";

        ProfindaAPiService service = new ProfindaAPiService(userActions);

        when(userActions.getUserIdByEmail(userDoesNotExist, "123")).thenReturn(Long.valueOf(1));
        when(userActions.getUserIdByEmail(user2, "123")).thenReturn(Long.valueOf(2));
        when(userActions.getUserIdByEmail(user3, "123")).thenReturn(Long.valueOf(3));

        when(userActions.updateUserById(1,"123")).thenReturn(new MockedHttpResponse().getMockedResponse(404)) ;
        when(userActions.updateUserById(2,"123")).thenReturn(new MockedHttpResponse().getMockedResponse(200)) ;
        when(userActions.updateUserById(3,"123")).thenReturn(new MockedHttpResponse().getMockedResponse(200)) ;

        List<String> emails = new LinkedList<>();
        emails.add(userDoesNotExist);
        emails.add(user2);
        emails.add(user3);

        int numberOfSuccessful = service.updateByEmail("123", emails);

        assertThat(numberOfSuccessful, equalTo(2));
    }
}