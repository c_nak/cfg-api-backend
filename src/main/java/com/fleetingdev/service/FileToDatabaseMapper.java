package com.fleetingdev.service;

import com.fleetingdev.model.ProfindaCSVFile;

import java.sql.SQLException;

public interface FileToDatabaseMapper {
  ProfindaCSVFile updateUsersWhoCompletedCourse(String filename) throws SQLException;
  ProfindaCSVFile updateUserPermissions(String filename, int permissionId) throws SQLException;
}
