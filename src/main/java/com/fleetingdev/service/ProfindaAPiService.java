package com.fleetingdev.service;

import com.Utils;
import com.fleetingdev.SpringBootWebApplication;
import com.profindaapi.Authentication;
import com.profindaapi.Exceptions.UserNotFoundException;
import com.profindaapi.UserActions;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ProfindaAPiService {

    private UserActions userActions;
    private static final Logger logger = LoggerFactory.getLogger(SpringBootWebApplication.class);

    public ProfindaAPiService() {
        this.userActions = new UserActions();
    }

    public ProfindaAPiService(UserActions userActions) {
        this.userActions = userActions;
    }

    public String authenticate() throws Exception {

        logger.info("Authenticating user....");
        String email = System.getenv("PROFINDA_EMAIL");
        String password = System.getenv("PROFINDA_PASSWORD");
        if (email == null || password == null) {
            throw new Exception("email or password is null, please set your env vars");

        }
        try {
            HttpResponse authTokenResponse = new Authentication().authenticate(email, password);
            JSONObject jsonResponse = Utils.getResponseAsJsonObject(authTokenResponse);
            if (authTokenResponse.getStatusLine().getStatusCode() == 404) {
                throw new Exception("Auth token not found");
            }
            logger.info("Authentication token retrieved");
            return (String) jsonResponse.get("authentication_token");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<String> updatePermissionsByEmail(String authtoken, List<String> emails, int permissionGroupId) {
        List<String> successfulEmailsUpdated = new LinkedList<>();

        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        logger.info("\nBeginning imports: " + timeStamp);

        for (String email : emails) {
            try {
                Long userId = userActions.getUserIdByEmail(email, authtoken);
                HttpResponse response = userActions.updateUserPermissionById(null, userId, permissionGroupId, authtoken);

                if (response.getStatusLine().getStatusCode() == 200) {
                    successfulEmailsUpdated.add(email);
                    logger.info("Successful import of: " + email);
                    logger.debug(userId.toString());
                    logger.debug(response.toString());
                }

            } catch (UserNotFoundException e) {
                logger.warn("Failed import for email: " + email + " reason: User Not Found");
                logger.warn(e.getMessage());
                logger.debug(email);
                logger.debug("Emails size: " + emails.size());
                logger.debug("\nEmails as string: " + emails.toString());
            }
        }

        logger.info("Imports completed: " + successfulEmailsUpdated);
        return successfulEmailsUpdated;
    }

    public List<String> updateByEmail(String authtoken, List<String> emails) {
        List<String> successfulImports = new LinkedList<>();

        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

        logger.info("\nBeginning imports: " + timeStamp);

        for (String email : emails) {
            try {
                Long userId = userActions.getUserIdByEmail(email, authtoken);
                CloseableHttpResponse response = (CloseableHttpResponse) userActions.updateUserById(userId, authtoken);


                if (response.getStatusLine().getStatusCode() == 200) {
                    successfulImports.add(email);
                    logger.info("Successful import of: " + email);
                    logger.debug(userId.toString());
                    logger.debug(response.toString());
                }

                EntityUtils.consume(response.getEntity());
                response.getEntity().consumeContent();
                response.close();

            } catch (UserNotFoundException e) {
                logger.warn("Failed import for email: " + email + " reason: User Not Found");
                logger.warn(e.getMessage());
                logger.debug(email);
                logger.debug("Emails size: " + emails.size());
                logger.debug("\nEmails as string: " + emails.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        logger.info("Imports completed: " + successfulImports);
        return successfulImports;
    }
}
