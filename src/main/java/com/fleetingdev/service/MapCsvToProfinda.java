package com.fleetingdev.service;

import com.fleetingdev.model.ProfindaCSVFile;
import de.siegmar.fastcsv.reader.CsvContainer;
import de.siegmar.fastcsv.reader.CsvReader;
import org.apache.http.conn.ConnectionPoolTimeoutException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;

public class MapCsvToProfinda implements FileToDatabaseMapper {

    private ProfindaAPiService profindaAPiService = new ProfindaAPiService();

    @Override
    public ProfindaCSVFile updateUsersWhoCompletedCourse(String filename) throws SQLException {

        ProfindaCSVFile profindaCSV = null;
        try {
            CsvContainer csv = getCsvContainer(filename);

            profindaCSV = new ProfindaCSVFile(csv);

            String authtoken = profindaAPiService.authenticate();

            if (authtoken == null) {
                return null;
            }
            List numberOfSuccessfullyUpdatedEmails = profindaAPiService.updateByEmail(authtoken, profindaCSV.getEmails());

            profindaCSV.setNumberOfSuccessfulImports(numberOfSuccessfullyUpdatedEmails);

        } catch (ConnectionPoolTimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return profindaCSV;
    }

    @Override
    public ProfindaCSVFile updateUserPermissions(String filename, int permissionId) throws SQLException {

        ProfindaCSVFile profindaCSV = null;
        try {
            CsvContainer csv = getCsvContainer(filename);
            profindaCSV = new ProfindaCSVFile(csv);

            String authtoken = profindaAPiService.authenticate();

            if (authtoken == null) {
                return null;
            }
            List numberOfSuccessful = profindaAPiService.updatePermissionsByEmail(authtoken, profindaCSV.getEmails(), permissionId);

            profindaCSV.setNumberOfSuccessfulImports(numberOfSuccessful);

        } catch (ConnectionPoolTimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return profindaCSV;
    }

    private CsvContainer getCsvContainer(String filename) throws IOException {
        File file = new File(filename);
        CsvReader csvReader = new CsvReader();
        csvReader.setContainsHeader(true);
        return csvReader.read(file, StandardCharsets.UTF_8);
    }
}
