package com.fleetingdev.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Cedric Kisema
 */
public class DBConnection {
  public static Connection getConnection() throws SQLException, ClassNotFoundException {
    Class.forName("com.mysql.jdbc.Driver");

    return DriverManager.getConnection("jdbc:mysql://localhost:3306/cfg-test",
            "root", "");
  }
}
