package com.fleetingdev.model;

import de.siegmar.fastcsv.reader.CsvContainer;
import de.siegmar.fastcsv.reader.CsvRow;

import java.util.LinkedList;
import java.util.List;

public class ProfindaCSVFile {

    private CsvContainer csv;
    private List<String> numberOfSuccessfulImports;

    public ProfindaCSVFile() { }

    public ProfindaCSVFile(CsvContainer csv) {
        this.csv = csv;
    }

    public List<String> getNumberOfSuccessfulImport() {
        return numberOfSuccessfulImports;
    }

    public List getEmails() {
        List<String> emails = new LinkedList<>();
        for (CsvRow row : csv.getRows()) {
             emails.add(row.getField("email"));
        }
        return emails;
    }

    public void setNumberOfSuccessfulImports(List<String> successfullyUpdatedEmails) {
        this.numberOfSuccessfulImports = successfullyUpdatedEmails;
    }
}
