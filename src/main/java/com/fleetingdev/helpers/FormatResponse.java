package com.fleetingdev.helpers;

import java.util.List;

public class FormatResponse {
    public static String format(List<String> numberOfSuccessfulImport) {
        StringBuilder response = new StringBuilder();

        for(String email : numberOfSuccessfulImport) {
            response.append("\n " + email + "\n");
        }
        return response.toString();
    }
}
