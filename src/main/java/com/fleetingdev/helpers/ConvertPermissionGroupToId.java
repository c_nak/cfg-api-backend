package com.fleetingdev.helpers;

public class ConvertPermissionGroupToId {
    public int permissionGroupToId(String permissionGroup) {
        int permissionGroupId;
        switch (permissionGroup.toLowerCase()) {
            case "cfg_team":
                permissionGroupId = 169;
                break;
            case "corporate":
                permissionGroupId = 168;
                break;
            case "alumni_students":
                permissionGroupId = 167;
                break;
            case "community":
                permissionGroupId = 166;
                break;
            default:
                permissionGroupId = 136;
                break;
        }
        return permissionGroupId;
    }
}
