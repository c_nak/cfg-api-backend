package com.fleetingdev.controller;

import com.fleetingdev.SpringBootWebApplication;
import com.fleetingdev.helpers.ConvertPermissionGroupToId;
import com.fleetingdev.helpers.FormatResponse;
import com.fleetingdev.model.ProfindaCSVFile;
import com.fleetingdev.service.FileToDatabaseMapper;
import com.fleetingdev.service.MapCsvToProfinda;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;

@CrossOrigin(origins = "http://localhost:5000")
@Controller
public class UploadController {

    //Save the uploaded file to this folder
    private FileToDatabaseMapper csvMapper = new MapCsvToProfinda();
    private static final Logger logger = LoggerFactory.getLogger(SpringBootWebApplication.class);

    @GetMapping("/")
    public String index() {
        return "upload";
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(path="/updatePermissionById", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object>  updatePermissionBy(@RequestParam("file") MultipartFile file, @RequestParam(value = "permissionGroup")
            String permissionGroup) {

        if (file.isEmpty()) {
            return new ResponseEntity<>("Please select a file to upload", HttpStatus.BAD_REQUEST);
        }
        try {
            byte[] bytes = file.getBytes();
            String uploaderFolderPath = new File(".").getCanonicalPath();
            Path path = Paths.get(uploaderFolderPath + "/" + file.getOriginalFilename());
            Files.write(path, bytes);

            ConvertPermissionGroupToId convert = new ConvertPermissionGroupToId();
            int permissionId = convert.permissionGroupToId(permissionGroup);

            ProfindaCSVFile csvFile = csvMapper.updateUserPermissions(path.toString(), permissionId);

            logger.info("Temp folder is set to: " + path);

            if (csvFile == null || csvFile.getNumberOfSuccessfulImport().size() == 0) {
                return new ResponseEntity<>("No students were updated, no emails were found.",HttpStatus.OK);
            }

            return new ResponseEntity<>(String.valueOf("You uploaded " + csvFile.getNumberOfSuccessfulImport() + " Student(s)"),HttpStatus.OK);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Successfully Uploaded",HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(path="/upload", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> singleFileUpload(@RequestParam("file") MultipartFile file ) {
        if (file.isEmpty()) {
            return new ResponseEntity<>("Please select a file to upload",HttpStatus.BAD_REQUEST);
        }

        try {
            byte[] bytes = file.getBytes();
            String uploaderFolderPath = new File(".").getCanonicalPath();
            Path path = Paths.get(uploaderFolderPath + "/" + file.getOriginalFilename());
            Files.write(path, bytes);

            ProfindaCSVFile csvFile = csvMapper.updateUsersWhoCompletedCourse(path.toString());

            logger.info("Temp folder is set to: " + path);

            if (csvFile == null || csvFile.getNumberOfSuccessfulImport().size() == 0) {
                return new ResponseEntity<>("No students were updated, no emails were found.",HttpStatus.OK);
            }

            String formattedResponse =  FormatResponse.format(csvFile.getNumberOfSuccessfulImport());

            return new ResponseEntity<>(String.valueOf("You updated the following student, \n" + formattedResponse + " Student(s)"),HttpStatus.OK);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>("Successfully Uploaded",HttpStatus.OK);
    }

    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }
}